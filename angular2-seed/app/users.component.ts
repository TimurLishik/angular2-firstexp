import  {Component, OnInit} from 'angular2/core'
import {PostService} from './user.service'
import {HTTP_PROVIDERS} from 'angular2/http'
import 'rxjs/add/operator/map'

@Component({
    template: `<h1>Users123</h1>
    
    `,
    providers: [PostService, HTTP_PROVIDERS]
    
})

export class UsersComponent implements OnInit{
    constructor(private _postService: PostService){
    }

    ngOnInit() {
        this._postService.getUsers()
        .subscribe()
    }

}