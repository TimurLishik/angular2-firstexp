import {Component,} from 'angular2/core';
import {NavBarComponent} from './navbar.component'
import {ROUTER_DIRECTIVES,RouteConfig} from 'angular2/router'
import {HomeComponent} from './home.component'
import {UsersComponent} from './users.component'
import {PostsComponent} from './posts.component'

@RouteConfig([
    {path: '/', name: 'Home', component: HomeComponent},
    {path: 'users', name: 'Users', component: UsersComponent},
    {path: 'posts', name: 'Posts', component: PostsComponent},
    {path: '/*others', name: 'Other', redirectTo:['Home']}
    
])

@Component({
    selector: 'my-app',
    template:`
    <navbar></navbar>
    <div class="container">
        <router-outlet></router-outlet>
    </div> 
    
    `
    ,
    directives:[NavBarComponent, ROUTER_DIRECTIVES]
})
export class AppComponent {

}