import {Http} from 'angular2/http'
import 'rxjs/add/operator/map'
import {Injectable} from 'angular2/core'

@Injectable()

export class PostService {
    constructor(private _http: Http){

    }

    getUsers(){
        return this._http.get("https://jsonplaceholder.typicode.com/users")
        .map(res=>res.json())
    }
}
